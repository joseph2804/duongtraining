﻿using Dapper;
using GasStationTraining.Models;
using GasStationTraining.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Controllers
{
    
    public class UserController : Controller
    {
        private readonly IDapper _dapper;
        public UserController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var userId = Convert.ToInt64(HttpContext.Session.GetString("UserId"));
            ViewBag.userId = userId;
            return View();
        }

        [HttpPost]
        public bool Login([FromBody] User user) 
        {
            string query = $"select * from Users where Email = '{user.Email}' and Password = '{user.Password}' and UserType = '00001'";
            using (var db = _dapper.GetDbconnection())
            {
                var aUser = db.QueryAsync<User>(query).Result.FirstOrDefault();
                if (aUser != null)
                {
                    const string sessionKey = "UserId";
                    var serialisedKey = aUser.UserId;
                    HttpContext.Session.SetString(sessionKey, serialisedKey.ToString());
                    return true;
                }
            }
            return false;
        }
    }
}

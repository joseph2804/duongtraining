﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models.CustomModels.Paging
{
    public class PagingModel
    {
        public int CurrPage { get; set; }
        public int PageSize { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models
{
    public class District
    {
        public long DistrictId { get; set; }
        public string? DistrictName { get; set; }
    }
}

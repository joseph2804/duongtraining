﻿
$(document).ready(function () {
    var temp = 0;
    var gasId;
    $('body').on('click', '#delete-triger', function () {
        gasId = $(this).attr('data-gasId');
    })
    $('body').on('click', '#delete', function () {
        $.ajax({
            type: 'DELETE',
            url: URL_DELETE + '?id='+gasId,
            headers: {
                'Content-Type': 'application/json'
            },
            success: function (data, status, res) {
                if (res.status == 200 && data) {
                    console.log(data);
                    location.reload();
                }
            }
        })
    })
    if (+$('#totalPage').val() <= 1) {
        $('.paging').hide();
    }
    $('#search').click(function () {
        temp = 1;
        $('#currPage').val('0');
        var gasName = $('#gasName').val();
        if (gasName == '')
            gasName = '-1';
        var queryType = "";
        $("input:checked").each(function (index, element) {
            console.log(element);
            queryType += element.id + " ";
        })
        queryType = queryType.trim();
        if (queryType == '')
            queryType = '-1';
        var district = $('#district').val();
        if (district == '')
            district = -1;
        url = `http://localhost:58860/GasStation/Search` + "?gasName=" + gasName + "&gasType=" + queryType + "&districtId=" + district;
        temp = url;
        $.ajax({
            type: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json'
            },
            success: function (data, status, res) {
                if (res.status == 200) {
                    console.log(data);
                    localStorage.setItem('list', JSON.stringify(data));
                    let totalPage = Math.ceil(parseFloat(data.length) / 10);
                    if (totalPage > 1) {
                        $('#totalPage').val(totalPage + '');
                        $('.paging').show();
                        $('#page').text(+$('#currPage').val() + 1 + '');
                        $('#previous').addClass('text-muted');
                        $('#first').addClass('text-muted');
                        $('#next').removeClass('text-muted');
                        $('#last').removeClass('text-muted');
                    } else {
                        $('.paging').hide();
                    }
                    
                    $('tbody').empty();
                    if (data.length != 0) {
                        if (totalPage > 1) {
                            for (let i = 0; i < 10; i++) {
                                //console.log(item);
                                let item = data[i];
                                var icon = `<i class="fas fa-certificate text-muted"></i>`;
                                if (item.rating == '00001') {
                                    icon = `<i class="fas fa-certificate text-danger"></i>`;
                                } else if (item.rating == '00002') {
                                    icon = `<i class="fas fa-certificate text-primary"></i>`;
                                }
                                $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                            }
                        } else {
                            data.forEach(function (item) {
                                var icon = `<i class="fas fa-certificate text-muted"></i>`;
                                if (item.rating == '00001') {
                                    icon = `<i class="fas fa-certificate text-danger"></i>`;
                                } else if (item.rating == '00002') {
                                    icon = `<i class="fas fa-certificate text-primary"></i>`;
                                }
                                $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                            })
                        }

                        
                    } else {
                        $('tbody').append(`<tr><td class="text-center text-warning" colspan="6">該当するデータはありません</td></tr>`);
                    }
                }
               
            }
        })
    });
    if (+$('#currPage').val() == +$('#totalPage').val() - 1) {
        $('#next').addClass('text-muted');
        $('#last').addClass('text-muted');
    }
    $('body').on('click', '#next', function () {
        let currPage = +$('#currPage').val();
        currPage++;
        let totalPage = +$('#totalPage').val();
        $('#previous').removeClass('text-muted');
        $('#first').removeClass('text-muted');
        if (currPage == totalPage - 1) {
            $('#next').addClass('text-muted');
            $('#last').addClass('text-muted');
        }
        let url = URL_INDEXGAS + '?CurrPage='+currPage;
        if (currPage < totalPage) {
            
            if (temp == 1) {
                $('tbody').empty();
                let list = JSON.parse(localStorage.getItem('list'));
                for (let i = (currPage -1 )* 10; i < (currPage - 1) * 10 + 10; i++) {
                    let item = list[i];
                    var icon = `<i class="fas fa-certificate text-muted"></i>`;
                    if (item.rating == 'Good') {
                        icon = `<i class="fas fa-certificate text-danger"></i>`;
                    } else if (item.rating == 'Mid') {
                        icon = `<i class="fas fa-certificate text-primary"></i>`;
                    }
                    $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                }
            }
            else
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val(currPage + '');
                        console.log(data);
                        $('tbody').empty();
                        data.forEach(function (item) {
                            console.log(item);
                            var icon = `<i class="fas fa-certificate text-muted"></i>`;
                            if (item.rating == 'Good') {
                                icon = `<i class="fas fa-certificate text-danger"></i>`;
                            } else if (item.rating == 'Mid') {
                                icon = `<i class="fas fa-certificate text-primary"></i>`;
                            }
                            $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                        });
                    }

                }
            })
            $('#page').text(currPage + 1 + '');
        }
    })

    $('body').on('click', '#previous', function () {
        let currPage = +$('#currPage').val();
        currPage--;
        $('#next').removeClass('text-muted');
        $('#last').removeClass('text-muted');
        if (currPage == 0) {
            $('#previous').addClass('text-muted');
            $('#first').addClass('text-muted');
        }
        let url = URL_INDEXGAS + `?CurrPage=${currPage}`;
        if (currPage >= 0) {
            $('#page').text(currPage + 1 + '');
            if (temp == 1) {
                $('tbody').empty();
                let list = JSON.parse(localStorage.getItem('list'));
                for (let i = (currPage - 1) * 10; i < (currPage - 1) * 10 + 10; i++) {
                    let item = list[i];
                    var icon = `<i class="fas fa-certificate text-muted"></i>`;
                    if (item.rating == 'Good') {
                        icon = `<i class="fas fa-certificate text-danger"></i>`;
                    } else if (item.rating == 'Mid') {
                        icon = `<i class="fas fa-certificate text-primary"></i>`;
                    }
                    $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                }
            }
            else
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val(currPage + '');
                        console.log(data);
                        $('tbody').empty();
                        data.forEach(function (item) {
                            console.log(item);
                            var icon = `<i class="fas fa-certificate text-muted"></i>`;
                            if (item.rating == 'Good') {
                                icon = `<i class="fas fa-certificate text-danger"></i>`;
                            } else if (item.rating == 'Mid') {
                                icon = `<i class="fas fa-certificate text-primary"></i>`;
                            }
                            $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                        });
                    }

                }
            })
        }
    })
    $('body').on('click', '#first', function () {
        let currPage = +$('#currPage').val();
        
        let url = URL_INDEXGAS + `?CurrPage=0`;
        if (currPage >= 0) {
            $('#next').removeClass('text-muted');
            $('#last').removeClass('text-muted');
            $('#previous').addClass('text-muted');
            $('#first').addClass('text-muted');
            $('#page').text(currPage + 1 + '');
            if (temp == 1) {
                $('tbody').empty();
                let list = JSON.parse(localStorage.getItem('list'));
                for (let i = (currPage - 1) * 10; i < (currPage - 1) * 10 + 10; i++) {
                    let item = list[i];
                    var icon = `<i class="fas fa-certificate text-muted"></i>`;
                    if (item.rating == 'Good') {
                        icon = `<i class="fas fa-certificate text-danger"></i>`;
                    } else if (item.rating == 'Mid') {
                        icon = `<i class="fas fa-certificate text-primary"></i>`;
                    }
                    $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                }
            }else
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val('0');
                        console.log(data);
                        $('tbody').empty();
                        data.forEach(function (item) {
                            console.log(item);
                            var icon = `<i class="fas fa-certificate text-muted"></i>`;
                            if (item.rating == 'Good') {
                                icon = `<i class="fas fa-certificate text-danger"></i>`;
                            } else if (item.rating == 'Mid') {
                                icon = `<i class="fas fa-certificate text-primary"></i>`;
                            }
                            $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                        });
                    }

                }
            })
        }
    })
    $('body').on('click', '#last', function () {
        let currPage = +$('#currPage').val();
        let totalPage = +$('#totalPage').val();
        let url = URL_INDEXGAS + `?CurrPage=${totalPage - 1}`;
        if (currPage < totalPage) {
            $('#first').removeClass('text-muted');
            $('#previous').removeClass('text-muted');
            $('#last').addClass('text-muted');
            $('#next').addClass('text-muted');
            $('#page').text(totalPage + '');
            if (temp == 1) {
                $('tbody').empty();
                let list = JSON.parse(localStorage.getItem('list'));
                for (let i = (currPage - 1) * 10; i < (currPage - 1) * 10 + 10; i++) {
                    let item = list[i];
                    var icon = `<i class="fas fa-certificate text-muted"></i>`;
                    if (item.rating == 'Good') {
                        icon = `<i class="fas fa-certificate text-danger"></i>`;
                    } else if (item.rating == 'Mid') {
                        icon = `<i class="fas fa-certificate text-primary"></i>`;
                    }
                    $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                }
            } else
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val(totalPage - 1 + '');
                        console.log(data);
                        $('tbody').empty();
                        data.forEach(function (item) {
                            console.log(item);
                            var icon = `<i class="fas fa-certificate text-muted"></i>`;
                            if (item.rating == 'Good') {
                                icon = `<i class="fas fa-certificate text-danger"></i>`;
                            } else if (item.rating == 'Mid') {
                                icon = `<i class="fas fa-certificate text-primary"></i>`;
                            }
                            $('tbody').append(`<tr><td class="text-center"><a href="/GasStation/Feedback/${item.gasStationId}">` + item.gasStationName + `</td><td class="text-center">` + item.gasStationType + `</td><td class="text-center">` + item.longitude + `</td><td class="text-center">` + item.latitude + `</td><td class="text-center">` + icon + `</td><td class="text-center"><a href="/GasStation/EditView/${item.gasStationId}" class="text-warning"><i class="fas fa-edit" style="cursor:pointer;"> </i></a>|<i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#exampleModal" id="delete-triger" style="cursor:pointer;" data-gasId="${item.gasStationId}"></i>` + "</td></tr>");
                        });
                    }

                }
            })
        }
    })
    
});
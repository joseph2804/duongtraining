﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Controllers
{
    public class BaseController : Controller
    {   
        public BaseController() { }
        public IActionResult Index()
        {
            const string sessionKey = "UserId";
            var userId = HttpContext.Session.GetString(sessionKey);
            if(userId != null)
            {
                return Redirect("/GasStation/List");
            }
            return Redirect("/User/Index");
        }
    }
}

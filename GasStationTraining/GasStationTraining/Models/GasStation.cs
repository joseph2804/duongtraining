﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models
{
    public class GasStation
    {
        public long GasStationId { get; set; }
        public string GasStationName { get; set; }
        public string? Address { get; set; }
        public long District { get; set; }
        public string? OpeningTime { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string? Rating { get; set; }
        public DateTime InsertedAt { get; set; }
        public long InsertedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public long UpdateedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
        public long? DeletedBy { get; set; }
        public string? GasType { get; set; }
    }
}

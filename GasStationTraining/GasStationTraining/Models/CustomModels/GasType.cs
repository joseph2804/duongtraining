﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models.CustomModels
{
    public class GasType
    {
        public long GasStationId { get; set; }
        public string TypeText { get; set; }
    }
}

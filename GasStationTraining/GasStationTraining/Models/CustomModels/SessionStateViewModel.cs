﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models.CustomModels
{
    public class SessionStateViewModel
    {
        public string UserId { get; set; }
    }
}

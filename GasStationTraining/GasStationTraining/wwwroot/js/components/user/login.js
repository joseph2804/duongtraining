﻿$(document).ready(function () {
    if ($('#userId').val() == 0) {
        $('header').hide();
    }
    function checkValid() {
        var check = true;
        if (!$('#user-name').val()) {
            $('#user-name-err').text('を入力してください');
            check = false;
        }
        if (!$('#password').val()) {
            $('#password-err').text('を入力してください');
            check = false;
        }
        return check;
    }
    $('body').on('focus', '#user-name', function () {
        $('#user-name-err').text('');
    })
    $('body').on('focus', '#password', function () {
        $('#password-err').text('');
    })
    $('#submit').click(function (e) {
        e.preventDefault();

        if (checkValid()) {
            body = {
                Email: $('#user-name').val(),
                Password: $('#password').val()
            }
            $.ajax({
                type: 'POST',
                data: JSON.stringify(body),
                cache: false,
                url: URL_LOGIN,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200 && data == true) {
                        window.location.href = '/GasStation/index';
                    }
                    else {
                        $('#user-name-err').text('ログインに失敗しました。電子メールまたはパスワードを確認してください。');
                    }
                }
            });
        }
    })
})
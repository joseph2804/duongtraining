﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models
{
    public class GasStationGasType
    {
        public long GasStationGasTypeId { get; set; }
        public long? GasStationId { get; set; }
        public string? GasType { get; set; }
    }
}

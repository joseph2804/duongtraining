﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Utils.Filters.Actions
{
    public class LoginFilter: ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            const string sessionKey = "UserId";
            var userId = context.HttpContext.Session.GetString(sessionKey);
            if(userId == null)
            {
                context.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                    { "controller", "User" },
                    { "action", "Index" }
                });
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            Console.WriteLine("after executed");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models
{
    public class GasStationFeedback
    {
        public long GasStationFeedbackId { get; set; }
        public long GasStationId { get; set; }
        public string FeedBack { get; set; }
        public DateTime FeedbackAt { get; set; }
        public long? FeedbackBy { get; set; }
    }
}

﻿using Dapper;
using GasStationTraining.Models;
using GasStationTraining.Models.CustomModels;
using GasStationTraining.Services;
using GasStationTraining.Utils.Filters.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Controllers
{
    [TypeFilter(typeof(LoginFilter))]
    public class GasStationController : Controller
    {
        private readonly IDapper _dapper;
        public GasStationController(IDapper dapper)
        {
            _dapper = dapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            int PageSize = 10;
            List<GasStationList> gasStations = new List<GasStationList>();
            string query = $"select Temp.GasStationId, Temp.GasStationName, Temp.DistrictName, Temp.Longitude, Temp.Latitude, M.TypeText as Rating from M_Type M join ( select G.GasStationId, G.GasStationName, D.DistrictName, G.Longitude, G.Latitude, G.Rating from GasStation G join M_District D on G.District = D.DistrictId where DeletedBy is null ) as Temp on M.TypeCode = Temp.Rating and M.TypeType = 4 order by Temp.GasStationName";
            string queryTpyeText = $"select G.GasStationId, M.TypeText from M_Type M join GasStationGasType G on M.TypeCode = G.GasType and M.TypeType = 3";
            string queryDistrict = "select * from M_District";
            string queryTypeText1 = "select T.TypeText from M_Type T where T.TypeType = 3";
            using (var db = _dapper.GetDbconnection())
            {
                var temp = db.QueryAsync<GasStationList>(query).Result;
                ViewBag.totalPage = Math.Ceiling((double)temp.Count() / PageSize);
                gasStations = db.QueryAsync<GasStationList>(query).Result.Skip(0).Take(PageSize).ToList();
                ViewBag.gasTypes = db.QueryAsync<GasType>(queryTpyeText).Result.ToList();
                ViewBag.districts = db.QueryAsync<District>(queryDistrict).Result.ToList();
                ViewBag.types = db.QueryAsync<M_Type>(queryTypeText1).Result.ToList();
            }
            return View(gasStations);
        } 

        [HttpGet]
        public List<GasStationList> Paging([FromQuery] int CurrPage)
        {
            int PageSize = 10;
            List<GasStationList> gasStations = new List<GasStationList>();
            string queryTpyeText = "select G.GasStationId, M.TypeText from M_Type M join GasStationGasType G on M.TypeCode = G.GasType and M.TypeType = 3";
            string query = $"select Temp.GasStationId, Temp.GasStationName, Temp.DistrictName, Temp.Longitude, Temp.Latitude, M.TypeText as Rating from M_Type M join ( select G.GasStationId, G.GasStationName, D.DistrictName, G.Longitude, G.Latitude, G.Rating from GasStation G join M_District D on G.District = D.DistrictId where DeletedBy is null) as Temp on M.TypeCode = Temp.Rating and M.TypeType = 4 order by Temp.GasStationName";
            using (var db = _dapper.GetDbconnection())
            {
                gasStations = db.Query<GasStationList>(query).Skip(CurrPage*PageSize).Take(PageSize).ToList();
                var gasTypes = db.Query<GasType>(queryTpyeText).ToList();
                foreach (var item in gasStations)
                {
                    string type = "";
                    foreach (var x in gasTypes)
                    {
                        if (item.GasStationId == x.GasStationId)
                        {
                            type += x.TypeText + ", ";
                        }
                    }
                    type = type.Remove(type.Length - 2);
                    item.GasStationType = type;
                }

            }
            return gasStations;
        }

        [HttpGet]
        public IActionResult Create()
        {
            string queryTypeRating = "select T.TypeText, T.TypeCode from M_Type T where T.TypeType = 4";
            string queryDistrict = "select * from M_District order by DistrictName ASC";
            string queryTpyeGas = "select T.TypeText, T.TypeCode from M_Type T where T.TypeType = 3";
            using (var db = _dapper.GetDbconnection())
            {
                ViewBag.gasTypes = db.QueryAsync<M_Type>(queryTpyeGas).Result.ToList();
                ViewBag.districts = db.QueryAsync<District>(queryDistrict).Result.ToList();
                ViewBag.ratings = db.QueryAsync<M_Type>(queryTypeRating).Result.ToList();
            }
            return View();
        }

        [HttpGet()]
        public GasStation GetById([FromQuery] long id)
        {
            string query = $"select * from GasStation where GasStationId = {id}";
            using (var db = _dapper.GetDbconnection())
            {
                return db.QueryAsync<GasStation>(query).Result.FirstOrDefault();
            }
        }

        [HttpPost]
        public IActionResult AddNew([FromBody] GasStation gasStation)
        {

            var userId = Convert.ToInt64(HttpContext.Session.GetString("UserId"));
            DateTime timeNow = DateTime.Now;
            var gastype = gasStation.GasType;
            var arrType = gastype.Split(" ");
            string queryInsert = "insert into GasStationGasType(GasStationId, GasType) values";
            string queryIdMax = "select Max(GasStationId) as GasStationId from GasStation";
            string queryAdd = $"Insert Into GasStation (GasStationName, Longitude, Latitude, District, Address, OpeningTime, Rating, InsertedAt, InsertedBy, UpdatedAt, UpdatedBy)values(N'{gasStation.GasStationName}', {gasStation.Longitude}, {gasStation.Latitude}, {gasStation.District}, N'{gasStation.Address}', '{gasStation.OpeningTime}', '{gasStation.Rating}', '{timeNow}', {userId}, '{timeNow}', {userId})";
            using (var db = _dapper.GetDbconnection())
            {
                db.Query<GasStation>(queryAdd);
                var gasId = db.Query<GasStation>(queryIdMax).FirstOrDefault().GasStationId;
                foreach (var item in arrType)
                {
                    queryInsert += $"({gasId}, '{item}'),";
                }
                queryInsert = queryInsert.Remove(queryInsert.Length - 1);
                db.Query(queryInsert);
            }
            return CreatedAtAction("GetById", new { id = gasStation.GasStationId }, gasStation);
        }

        [HttpGet]
        public bool checkExist([FromQuery] string gasName, [FromQuery] long id)
        {
            string query = $"select * from GasStation where GasStationName = N'{gasName}'";
            using (var db = _dapper.GetDbconnection())
            {
                var item = db.QueryAsync<GasStation>(query).Result.FirstOrDefault();
                if (item != null && item.GasStationId != id )
                    return true;
            }
            return false;
        }
        [HttpGet]
        public List<GasStationList> Search([FromQuery] string gasName, [FromQuery] string gasType, [FromQuery] long districtId)
        {
            List<GasType> gasTypes = new List<GasType>();
            string queryTpyeText = "select G.GasStationId, M.TypeText from M_Type M join GasStationGasType G on M.TypeCode = G.GasType and M.TypeType = 3";
            var queryId = "";
            var queryType = "";
            var queryDistrict = "";
            if (gasName != "-1")
            {
                queryId += $"G.GasStationName like N'%{gasName}%'" ;
                
            }
            
            if (gasType != "-1")
            {
                string[] types = gasType.Split(" ");
                foreach (var item in types)
                {
                    queryType += $"M.TypeText = '{ item}' or ";
                }
                queryType = queryType.Remove(queryType.Length - 4);
                queryType = $"(GT.GasType in (select M.TypeCode from M_Type M where(" + queryType + ") and M.TypeType = 3)) ";
                if (queryId != "")
                {
                    queryType = " and " + queryType;
                }
            }
            if (districtId != -1)
            {
                queryDistrict += $"G.District = {districtId}";
                if (queryType != "" || queryId != "")
                {
                    queryDistrict = " and " + queryDistrict;
                }
            }

            if (queryId != "" || queryDistrict != "" || queryType != "")
            {
                queryId = "where " + queryId;
            }
            List<GasStationList> gasStations = new List<GasStationList>();
            string query = $"select DISTINCT TEMP.GasStationId, TEMP.GasStationName, TEMP.Longitude, TEMP.Latitude, TEMP.Rating, D.DistrictName  from M_District D join( select G.GasStationId, G.GasStationName, G.Longitude, G.Latitude, G.Rating, G.District from GasStation G join GasStationGasType GT on G.GasStationId = GT.GasStationId  " + queryId + queryType + queryDistrict+") TEMP on D.DistrictId = TEMP.District";
            var temp = query;
            using (var db = _dapper.GetDbconnection())
            {
                gasStations = db.Query<GasStationList>(query).ToList();
                gasTypes = db.Query<GasType>(queryTpyeText).ToList();
                foreach (var item in gasStations)
                {
                    string type = "";
                    foreach (var x in gasTypes)
                    {
                        if (item.GasStationId == x.GasStationId)
                        {
                            type += x.TypeText + ", ";
                        }
                    }
                    type = type.Remove(type.Length - 2);
                    item.GasStationType = type;
                }
            }
            return gasStations;
        }

        [HttpDelete]
        public bool Delete([FromQuery] long id)
        {
            var curUser = HttpContext.Session.GetString("UserId");
            string query = $"update GasStation set DeletedAt = GETDATE(), DeletedBy = {curUser} where GasStationId  = {id}";
            using (var db = _dapper.GetDbconnection())
            {
                try
                {
                    db.QueryAsync(query);
                    return true;
                } catch (Exception e)
                {
                    return false;
                }
            }
        }
        [HttpGet]
        public IActionResult EditView(long id)
        {
            string queryById = $"select * from GasStation where GasStationId = {id}";
            string queryGasTypeById = $"select * from GasStationGasType  where GasStationId = {id}";
            string queryTypeRating = "select T.TypeText, T.TypeCode from M_Type T where T.TypeType = 4";
            string queryDistrict = "select * from M_District order by DistrictName ASC";
            string queryTypeGas = "select T.TypeText, T.TypeCode from M_Type T where T.TypeType = 3";
            using (var db = _dapper.GetDbconnection())
            {
                ViewBag.aGasStation = db.QueryAsync<GasStation>(queryById).Result.FirstOrDefault();
                ViewBag.gasTypeById = db.QueryAsync<GasStationGasType>(queryGasTypeById).Result.ToList();
                ViewBag.gasTypes = db.QueryAsync<M_Type>(queryTypeGas).Result.ToList();
                ViewBag.districts = db.QueryAsync<District>(queryDistrict).Result.ToList();
                ViewBag.ratings = db.QueryAsync<M_Type>(queryTypeRating).Result.ToList();
            }
            return View();
        }

        [HttpPost]
        public IActionResult Update([FromBody] GasStation gasStation)
        {
            var curUser = Convert.ToInt64(HttpContext.Session.GetString("UserId"));
            var gastype = gasStation.GasType;
            var arrType = gastype.Split(" ");
            string queryUpdate = $"update GasStation set GasStationName = '{gasStation.GasStationName}', Longitude ={gasStation.Longitude} , Latitude = {gasStation.Latitude}, District = {gasStation.District}, Address = '{gasStation.Address}', OpeningTime = '{gasStation.OpeningTime}', Rating = '{gasStation.Rating}', UpdatedAt = GETDATE(), UpdatedBy = {curUser} where GasStationId = {gasStation.GasStationId}";
            string queryDeleteType = $"DELETE FROM GasStationGasType WHERE GasStationId = {gasStation.GasStationId}";
            string queryInsert = "insert into GasStationGasType(GasStationId, GasType) values";
            foreach (var item in arrType)
            {
                queryInsert += $"({gasStation.GasStationId}, '{item}'),";
            }
            queryInsert = queryInsert.Remove(queryInsert.Length - 1);
            using (var db = _dapper.GetDbconnection())
            {
                try
                {
                    db.Query(queryDeleteType);
                    db.Query(queryUpdate);
                    db.Query(queryInsert);
                    return Ok();
                }catch(Exception e)
                {
                    return NoContent();
                }
            }
        }

        [HttpGet]
        public IActionResult Feedback(long id)
        {
            int pageSize = 10;
            string queryById = $"select * from GasStation where GasStationId = {id}";
            string queryGasTypeById = $"select * from GasStationGasType  where GasStationId = {id}";
            string queryTypeRating = "select T.TypeText, T.TypeCode from M_Type T where T.TypeType = 4";
            string queryDistrict = "select * from M_District order by DistrictName ASC";
            string queryTypeGas = "select T.TypeText, T.TypeCode from M_Type T where T.TypeType = 3";
            string queryFeedback = $"select * from GasStationFeedback where GasStationId = {id}";
            using (var db = _dapper.GetDbconnection())
            {
                ViewBag.aGasStation = db.QueryAsync<GasStation>(queryById).Result.FirstOrDefault();
                ViewBag.gasTypeById = db.QueryAsync<GasStationGasType>(queryGasTypeById).Result.ToList();
                ViewBag.gasTypes = db.QueryAsync<M_Type>(queryTypeGas).Result.ToList();
                ViewBag.districts = db.QueryAsync<District>(queryDistrict).Result.ToList();
                ViewBag.ratings = db.QueryAsync<M_Type>(queryTypeRating).Result.ToList();
                var query = db.QueryAsync<GasStationFeedback>(queryFeedback).Result;
                ViewBag.feedbacks = query.Skip(0).Take(pageSize).ToList();
                var totalRecords = query.Count();
                ViewBag.totalPage = Math.Ceiling((double)totalRecords/pageSize);
                ViewBag.gasId = id;
            }
            return View();
        }

        [HttpGet]
        public List<GasStationFeedback> PagingFeedback([FromQuery]long id, [FromQuery] int CurrPage)
        {
            int pageSize = 10;
            string queryFeedback = $"select * from GasStationFeedback where GasStationId = {id}";
            using (var db = _dapper.GetDbconnection())
            {
                return db.QueryAsync<GasStationFeedback>(queryFeedback).Result.Skip(CurrPage * pageSize).Take(pageSize).ToList();
            }
        }
        
    }
}

﻿$(document).ready(function () {
    if (+$('#totalPage').val() <= 1) {
        $('.paging').hide();
    }
    
    $('body').on('click', '#next', function () {
        let currPage = +$('#currPage').val();
        let gasId = +$('#gasId').val();
        currPage++;
        let totalPage = +$('#totalPage').val();
        $('#previous').removeClass('text-muted');
        $('#first').removeClass('text-muted');
        if (currPage == totalPage - 1) {
            $('#next').addClass('text-muted');
            $('#last').addClass('text-muted');
        }
        let url = URL_PAGINGFEEDBACK + '?id='+gasId+'&CurrPage='+currPage;
        if (currPage < totalPage) {
            $('#page').text(currPage + 1 + '');
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val(currPage + '');
                        console.log(data);
                        $('#feedback-area').empty();
                        data.forEach(function (item) {
                            var datetime = item.feedbackAt.split('T')[0];
                            var year = datetime.split('-')[0];
                            var month = datetime.split('-')[1];
                            var day = datetime.split('-')[2];
                            console.log(datetime);
                            $('#feedback-area').append(`<label for="">${year}年${month}月${day}日</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" aria-describedby="helpId" value="${item.feedBack}" readonly>`);
                        });
                    }

                }
            })
        }
    })
    $('body').on('click', '#previous', function () {
        let currPage = +$('#currPage').val();
        let gasId = +$('#gasId').val();
        currPage--;
        $('#next').removeClass('text-muted');
        $('#last').removeClass('text-muted');
        if (currPage == 0) {
            $('#previous').addClass('text-muted');
            $('#first').addClass('text-muted');
        }
        let url = URL_PAGINGFEEDBACK + `?id=${gasId}&CurrPage=${currPage}`;
        if (currPage >= 0) {
            $('#page').text(currPage + 1 + '');
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val(currPage + '');
                        console.log(data);
                        $('#feedback-area').empty();
                        data.forEach(function (item) {
                            var datetime = item.feedbackAt.split('T')[0];
                            var year = datetime.split('-')[0];
                            var month = datetime.split('-')[1];
                            var day = datetime.split('-')[2];
                            console.log(datetime);
                            $('#feedback-area').append(`<label for="">${year}年${month}月${day}日</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" aria-describedby="helpId" value="${item.feedBack}" readonly>`);
                        });
                    }

                }
            })
        }
    })
    $('body').on('click', '#last', function () {
        let currPage = +$('#currPage').val();
        let gasId = +$('#gasId').val();
        let totalPage = +$('#totalPage').val();
        let url = URL_PAGINGFEEDBACK + `?id=${gasId}&CurrPage=${totalPage - 1}`;
        if (currPage < totalPage) {
            $('#first').removeClass('text-muted');
            $('#previous').removeClass('text-muted');
            $('#last').addClass('text-muted');
            $('#next').addClass('text-muted');
            $('#page').text(totalPage + '');
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val(totalPage - 1 + '');
                        console.log(data);
                        $('#feedback-area').empty();
                        data.forEach(function (item) {
                            var datetime = item.feedbackAt.split('T')[0];
                            var year = datetime.split('-')[0];
                            var month = datetime.split('-')[1];
                            var day = datetime.split('-')[2];
                            console.log(datetime);
                            $('#feedback-area').append(`<label for="">${year}年${month}月${day}日</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" aria-describedby="helpId" value="${item.feedBack}" readonly>`);
                        });
                    }

                }
            })
        }
    })
    $('body').on('click', '#first', function () {
        let currPage = +$('#currPage').val();
        let gasId = +$('#gasId').val();
        let url = URL_PAGINGFEEDBACK + `?id=${gasId}&CurrPage=0`;
        if (currPage >= 0) {
            $('#next').removeClass('text-muted');
            $('#last').removeClass('text-muted');
            $('#previous').addClass('text-muted');
            $('#first').addClass('text-muted');
            $('#page').text(currPage + 1 + '');
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200) {
                        $('#currPage').val('0');
                        console.log(data);
                        $('#feedback-area').empty();
                        data.forEach(function (item) {
                            var datetime = item.feedbackAt.split('T')[0];
                            var year = datetime.split('-')[0];
                            var month = datetime.split('-')[1];
                            var day = datetime.split('-')[2];
                            console.log(datetime);
                            $('#feedback-area').append(`<label for="">${year}年${month}月${day}日</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" aria-describedby="helpId" value="${item.feedBack}" readonly>`);
                        });
                    }

                }
            })
        }
    })
    $('body').on('click', '#back', function () {
        window.location.href = '/GasStation';
    })
})
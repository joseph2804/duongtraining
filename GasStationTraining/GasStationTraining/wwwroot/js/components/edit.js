﻿$(document).ready(function () {
    function checkValid() {
        var check = true;
        if (!$('#name-gas').val()) {
            $('#name-gas-err').text('ガソリンスタンド名を入力してください');
            check = false;
        }
        if ($('#district').val() == 'none') {
            $('#district-err').text('地区を入力してください');
            check = false;
        }

        if (!$('#longitude').val()) {
            $('#longitude-err').text('Longitudeを入力してください');
            check = false;
        } else {
            let rgx = new RegExp('^[0-9]*.[0-9]*$');
            if (!rgx.test($('#longitude').val())) {
                $('#longitude-err').text('longitude only float');
                check = false;
            }
        }
        if (!$('#latitude').val()) {
            $('#latitude-err').text('Latitudeを入力してください');
            check = false;
        }
        else {
            let rgx = new RegExp('^[0-9]*.[0-9]*$');
            if (!rgx.test($('#latitude').val())) {
                $('#latitude-err').text('latitude only float');
                check = false;
            }
        }
        if ($('#name-gas').val().length > 100) {
            $('#name-gas-err').text('ガソリンスタンド名の長さは100文字を超えてはなりません');
            check = false;
        }
        if ($('#address').val().length > 200) {
            $('#address-err').text('住所の長さは200文字を超えてはなりません');
            check = false;
        }
        if ($('#open-time').val().length > 50) {
            $('#open-time-err').text('開館時間の長さは50文字を超えてはなりません');
            check = false;
        }
        if ($('input[type=checkbox]:checked').length < 1) {
            $('#type-err').text('種類を入力してください');
            check = false;
        }
        let value = parseFloat($('#longitude').val());
        if (value - Math.floor(value) == 0) {
            $('#longitude-err').text('longitude only float');
            check = false;
        }
        let value1 = parseFloat($('#latitude').val());
        if (value1 - Math.floor(value1) == 0) {
            $('#latitude-err').text('latitude only float');
            check = false;
        }
        return check;
    }
    let checkFloat;
    $('#longitude').blur(function () {
        let value = parseFloat($('#longitude').val());
        if (value - Math.floor(value) == 0) {
            $('#longitude-err').text('longitude only float');
            checkFloat = false;
        } else {
            $('#longitude-err').text('');
            checkFloat = true;
        }
    })
    $('#latitude').blur(function () {
        let value = parseFloat($('#latitude').val());
        if (value - Math.floor(value) == 0) {
            $('#latitude-err').text('latitude only float');
            checkFloat = false;
        } else {
            $('#latitude-err').text('');
            checkFloat = true;
        }
    })
    $('body').on('keydown', '#longitude', function (e) {
        let val = e.originalEvent.key;
        let reg = /^[a-zA-Z]$/;
        if (reg.test(val)) {
            e.preventDefault();
            checkFloat = false;
        }
    });
    $('body').on('keydown', '#latitude', function (e) {
        let val = e.originalEvent.key;
        let reg = /^[a-zA-Z]$/;
        if (reg.test(val)) {
            e.preventDefault();
            checkFloat = false;
        }
    });
    $('body').on('focus', '#district', function () {
        $('#district-err').text('');
    })
    $('body').on('focus', '#name-gas', function () {
        $('#name-gas-err').text('');
    })
    $('body').on('focus', '#longitude', function () {
        $('#longitude-err').text('');
    })
    $('body').on('focus', '#latitude', function () {
        $('#latitude-err').text('');
    })
    $('body').on('focus', '#open-time', function () {
        $('#open-time-err').text('');
    })
    $('body').on('focus', 'input[type=checkbox]', function () {
        $('#type-err').text('');
    })
    $('body').on('click', '#back-list', function (e) {
        e.preventDefault();
        window.location.href = '/GasStation/Index';
    })
    $('body').on('click', '#submit', function (e) {
        e.preventDefault();

        if (checkValid() && checkFloat) {
            let name = $('#name-gas').val();
            let gasId = +$('#gasId').val();
            url = URL_CHECKEXIST +'?gasName='+ name +'&id='+gasId;
            $.ajax({
                type: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                success: function (data, status, res) {
                    if (res.status == 200 && !data) {
                        console.log(data);
                        if (!data) {
                            let type = '';
                            $('input[type=checkbox]:checked').each(function (index, item) {
                                type += $(this).val() + ' ';
                            })
                            let body = {
                                GasStationId: +$('#gasId').val(),
                                GasStationName: $('#name-gas').val(),
                                GasType: type.trim(),
                                Longitude: +$('#longitude').val(),
                                Latitude: +$('#latitude').val(),
                                District: +$('#district').val(),
                                Address: $('#address').val(),
                                OpeningTime: $('#open-time').val(),
                                Rating: $('input[type=radio]:checked').val()
                            }
                            console.log(body);
                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(body),
                                url: URL_UPDATE,
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                success: function (data, status, res) {
                                    console.log(res.status);
                                    if (res.status == 200) {
                                        window.location.href = '/GasStation';
                                    }
                                    else {
                                        $('#name-gas-err').text('追加できません');
                                    }
                                }
                            });
                        } 
                    } else {
                        $('#name-gas-err').text('ガソリンスタンド名情報はすでに存在します。');
                    }

                }
            })
        }
    })
})
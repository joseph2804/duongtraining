﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models.CustomModels
{
    public class GasStationList
    {
        public long GasStationId { get; set; }
        public string GasStationName { get; set; }
        public string GasStationType { get; set; }
        public string DistrictName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Rating { get; set; }
    }
}

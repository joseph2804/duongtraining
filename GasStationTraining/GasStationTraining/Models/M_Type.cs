﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasStationTraining.Models
{
    public class M_Type
    {
        public long TypeId { get; set; }
        public string TypeCode { get; set; }
        public string TypeText { get; set; }
        public int TypeType { get; set; }

    }
}
